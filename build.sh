#!/usr/share/sh

docker build --tag 'fosnyos-builder' .
docker run -it --rm \
    -v "$STORAGE/src:/srv/src" \
    -v "$STORAGE/zips:/srv/zips" \
    -v "$STORAGE/logs:/srv/logs" \
    -v "$STORAGE/ccache:/srv/ccache" \
    -v "$STORAGE/keys:/srv/keys"\
    'fosnyos-builder'
