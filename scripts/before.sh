#!/usr/bin/bash
set -eEuo pipefail

# Remove some default LineageOS packages
rm -rf packages/apps/Jelly # Web Browser

# Apply the camera patch (Android 11)
# patch --quiet --force -p1 -i /root/patches/non_system_camera_a11.patch
patch --quiet --force -p1 -i /root/patches/non_system_camera_a13.patch
