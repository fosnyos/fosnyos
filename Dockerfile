FROM docker.io/lineageos4microg/docker-lineage-cicd:latest
LABEL maintainer="Vojtěch Fošnár <vfosnar@fosny.eu>"

COPY ./manifests /srv/local_manifests
COPY ./patches /root/patches
COPY ./scripts /root/userscripts

RUN chmod +x /root/userscripts/*

ENV DEVICE_LIST=lavender
ENV BRANCH_NAME=lineage-20.0
ENV OTA_URL=https://ota.android.vfosnar.cz/api
ENV SIGN_BUILDS=true
ENV CUSTOM_PACKAGES="AuroraServices AuroraStore FDroid FDroidPrivilegedExtension Mull ntfy"
ENV WITH_GMS=false
ENV BUILD_TYPE=user
